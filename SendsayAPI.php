<?php
namespace Sendsay;

class SendsayAPI
{
    const API_ADDRESS = "https://api.sendsay.ru/";
    const METHOD = "POST";
    const APIVERSION = 100;
    const IS_JSON = 1;


    /**
     * @var array. Данные для авторизации при запросе
     */
    private $authData = [
        "login" => "",
        "sublogin" => "",
        "passwd" => ""
    ];


    /**
     * @param $params = [ логин, сублогин, пароль ]
     */
    public function __construct($params)
    {
        $this->authData['login'] = $params['login'];
        $this->authData['sublogin'] = $params['sublogin'];
        $this->authData['passwd'] = $params['passwd'];
    }


    /**
     * @param array $data - Поля запроса для API, например $data = ["action" => "group.list"]
     * @return array|mixed
     */
    public function sendRequest($data = [])
    {
        $request = array_merge(["one_time_auth" => $this->authData,], $data);

        $PostFields = [
            "apiversion" => self::APIVERSION,
            "json" => self::IS_JSON,
            "request" => json_encode($request)
        ];

        $response = [];

        do {
            $address = self::API_ADDRESS;
            if (isset($response['REDIRECT'])) {
                $address .= $response['REDIRECT'];
            }
            $ch = curl_init($address);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, self::METHOD);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $PostFields);
            $response = json_decode(curl_exec($ch), 1);
            curl_close($ch);
        } while (isset($response['REDIRECT']));

        return $response;
    }
}